FROM ubuntu:18.04

ARG KLIPPER_VERSION="0.10.0"

RUN apt-get update
RUN apt-get install -y python-virtualenv virtualenv python-dev git
RUN apt-get install -y build-essential libffi-dev libncurses-dev libusb-dev
RUN apt-get install -y avrdude gcc-avr binutils-avr avr-libc stm32flash libnewlib-arm-none-eabi gcc-arm-none-eabi binutils-arm-none-eabi libusb-1.0

WORKDIR /klipper

RUN git clone --depth 1 --single-branch --branch v${KLIPPER_VERSION} https://github.com/KevinOConnor/klipper.git .

COPY bashrc /root/.bashrc
COPY make-firmware .

VOLUME /firmware

CMD ["/klipper/make-firmware"]

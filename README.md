# Klipper Firmware

```
mkdir -p firmware

docker run -it --rm -v $(pwd)/firmware:/firmware klipper-firmware:latest /bin/bash
```

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/RedSerenity/Docker/KlipperFirmware.git
git branch -M master
git push -uf origin master
```